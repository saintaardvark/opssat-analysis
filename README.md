# OPS-SAT analysis

First rough script of OPS-SAT telemetry taken from db.satnogs.org.
Data has been downloaded from db.satnogs.org.  Because this is being
used to track down a couple bugs, I'm keeping a copy of this data
available on my server; details are below.

# Procedure

Assuming a clean installation of Polaris in a virtualenv:

- In that virtualenv, edit `polaris/fetch/satellites.json` and add
  this:

```
@@ -233,6 +233,12 @@
     "decoder": "Uwe4",
     "normalizer": "Uwe4"
   },
+  {
+    "norad_id": "44878",
+    "name": "Opssat1",
+    "decoder": "opssat1",
+    "normalizer": "Opssat1"
+  },
   {
     "norad_id": "45598",
     "name": "Quetzal-1",
```

- Edit the Makefile and make sure that the `PVENV` variable points to
  the `activate` file for the virtualenv that contains Polaris.

- Assuming I've got everything right...you *should* be able to run:

```
make opssat-long
```

  ...to do the import and analysis for OPS-SAT.

**Note:** At this point, there are *two* problems you'll run into:

## Issue 169: Spaceweather files that can't be fetched will cause an error

The first problem is [this
issue](https://gitlab.com/librespacefoundation/polaris/polaris/-/issues/169):
spaceweather files that can't be fetched will result in an error when
it comes time to normalize the file.  I've worked around this by
applying the changes in [this
commit](https://gitlab.com/saintaardvark/polaris/-/commit/48e1336cfe67f88cbeb033a1e6241b94c435be01)
locally, which basically skips trying to include spaceweather columns
if there was a problem fetching them; the normalized frames file
produce by this (possibly quite wrong!) approach can be found
[here](https://saintaardvarkthecarpeted.com/random/polaris/opssat1-normalized_frames-long.json.gz). 

## OMG you need a lot of memory

The second problem is that this analysis uses a *lot* of
memory...like, a lot. Here's the brute-force way around the problem:

```
dd if=/dev/zero of=swapfile bs=1G count=65    # Yes, that's a 65 GB swap file.
sudo mkswap ./swapfile
sudo swapon ./swapfile
```

We have more work to do on memory management.

# A note on the `polaris learn` configuration, and problems with the `opssat-long` graph

When doing the `opssat-long` analysis -- that is, covering December
2019 to April 2021 -- the resulting graph had a very small number of
nodes.  To try and get around this, I added `learn.json`, the
configuration file for `polaris learn`.  This was copied from [our
integration
tests](https://gitlab.com/librespacefoundation/polaris/polaris/-/blob/master/robot/resources/learn_config.json),
**and may well not be suitable for this purpose**.  The resulting
graph has more nodes, but (nearly?) all of them seem to be
spaceweather nodes.  I'm not sure what's going on.

## Other targets

I've also other makefile targets:

  - `make short` will take the first (ie, oldest) 300 lines of the
    frames file and analyze that.  You can adjust that number by
    adjusting `SHORT_THRESHOLD` within the Makefile.
	
  - `make bug` will take the last (ie, newest) 300 lines of the frames
    file and analyze that.  This should trigger the [spaceweather bug
    mentioned
    above](https://gitlab.com/librespacefoundation/polaris/polaris/-/issues/169).

- You should be able to view the graph files locally like so:

```
make view-long
make view-short
```
