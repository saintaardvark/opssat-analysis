#
# Customize these variables:
#
# Path to virtualenv activation file.
PVENV=~/dev/src/polaris-clean/.venv/bin/activate

# These were obtained from
# https://db.satnogs.org/satellite/44878
#
# You'll need to log in and select the Data tab, then select the
# option to export all frames.
OPSSAT_FRAMES_ORIG=./44878-482-20210426T003943Z-all.csv
$(OPSSAT_FRAMES_ORIG): $(OPSSAT_FRAMES_ORIG).gz
	gunzip $(OPSSAT_FRAMES_ORIG).gz

$(OPSSAT_FRAMES_ORIG).gz:
	wget https://saintaardvarkthecarpeted.com/random/polaris/$(OPSSAT_FRAMES_ORIG).gz

OPSSAT_FRAMES_LONG=./44878-482-20210426T003943Z-long.csv
OPSSAT_FRAMES_SHORT=./44878-482-20210426T003943Z-short.csv
OPSSAT_FRAMES_BUG=./44878-482-20210426T003943Z-bug.csv

OPSSAT_NORMALIZED_LONG=./opssat1-normalized_frames-long.json
OPSSAT_NORMALIZED_SHORT=./opssat1-normalized_frames-short.json
OPSSAT_NORMALIZED_BUG=./opssat1-normalized_frames-bug.json

OPSSAT_GRAPH_LONG=./opssat1-graph-long.json
OPSSAT_GRAPH_SHORT=./opssat1-graph-short.json
OPSSAT_GRAPH_BUG=./opssat1-graph-bug.json

# $(OPSSAT_FRAMES_ORIG): $(OPSSAT_FRAMES).gz
# 	gunzip $(OPSSAT_FRAMES).gz

.PHONY: $(OPSSAT_FRAMES_LONG)
$(OPSSAT_FRAMES_LONG): $(OPSSAT_FRAMES_ORIG)
	cp $(OPSSAT_FRAMES_ORIG) $(OPSSAT_FRAMES_LONG)

SHORT_THRESHOLD=3000

.PHONY: $(OPSSAT_FRAMES_SHORT)
$(OPSSAT_FRAMES_SHORT): $(OPSSAT_FRAMES_ORIG)
	tail -$(SHORT_THRESHOLD) $(OPSSAT_FRAMES_ORIG) > $(OPSSAT_FRAMES_SHORT)

$(OPSSAT_FRAMES_BUG): $(OPSSAT_FRAMES_ORIG)
	head -$(SHORT_THRESHOLD) $(OPSSAT_FRAMES_ORIG) > $(OPSSAT_FRAMES_BUG)

long: $(OPSSAT_FRAMES_LONG) fetch-long learn-long
short: $(OPSSAT_FRAMES_SHORT) fetch-short learn-short
bug: $(OPSSAT_FRAMES_BUG) fetch-bug learn-bug

fetch-long:
	source $(PVENV) && \
	polaris fetch \
		--import_file `pwd`/$(OPSSAT_FRAMES_LONG) \
		--skip_normalizer \
		Opssat1 \
		$(OPSSAT_NORMALIZED_LONG)

learn-long:
	source $(PVENV) && \
		time polaris learn \
		-g $(OPSSAT_GRAPH_LONG) \
		--learn_config_file ./learn.json \
		--force_cpu \
		$(OPSSAT_NORMALIZED_LONG)

view-long:
	source $(PVENV) && \
		polaris viz \
		 $(OPSSAT_GRAPH_LONG) 

fetch-short:
	rm gl.log && \
	source $(PVENV) && \
	polaris fetch \
		--import_file `pwd`/$(OPSSAT_FRAMES_SHORT) \
		--skip_normalizer \
		Opssat1 \
		$(OPSSAT_NORMALIZED_SHORT)

learn-short:
	source $(PVENV) && \
		time polaris learn \
		-g $(OPSSAT_GRAPH_SHORT) \
		--force_cpu \
		$(OPSSAT_NORMALIZED_SHORT)

view-short:
	source $(PVENV) && \
		polaris viz \
		 $(OPSSAT_GRAPH_SHORT) 

fetch-bug:
	rm gl.log && \
	source $(PVENV) && \
	polaris fetch \
		--import_file `pwd`/$(OPSSAT_FRAMES_BUG) \
		--skip_normalizer \
		Opssat1 \
		$(OPSSAT_NORMALIZED_BUG)

learn-bug:
	source $(PVENV) && \
		time polaris learn \
		-g $(OPSSAT_GRAPH_BUG) \
		--force_cpu \
		$(OPSSAT_NORMALIZED_BUG)

